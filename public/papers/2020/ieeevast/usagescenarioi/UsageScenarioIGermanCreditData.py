#!/usr/bin/env python
# coding: utf-8

# # Usage Scenario I:  German Credit Bank
# 
# In this usage scenario, we utilize a pre-processed version (iForest) of the German Credit Data dataset from the UCI Machine Learning Repository.
# 
# ***Cite us***:  M. Popolin Neto and F. V. Paulovich, "Explainable Matrix - Visualization for Global and Local Interpretability of Random Forest Classification Ensembles," in IEEE Transactions on Visualization and Computer Graphics, vol. 27, no. 2, pp. 1427-1437, Feb. 2021, doi: 10.1109/TVCG.2020.3030354.
# 
# ***BibTeX:*** @article{PopolinNeto:2020:ExMatrix,
#     author={Popolin{ }Neto, Mário and Paulovich, Fernando V.},
#     journal={IEEE Transactions on Visualization and Computer Graphics}, 
#     title={Explainable Matrix - Visualization for Global and Local Interpretability of Random Forest Classification Ensembles}, 
#     year={2021},
#     volume={27},
#     number={2},
#     pages={1427-1437},
#     doi={10.1109/TVCG.2020.3030354}}
#     
# ***WARNING***: The code below must be used along with exmatrix package version **0.1.0**. 
# 
# Improvements and naive bugs have been addressed in new versions. Thus, such new versions will require specific changes in the code and will generate slightly different visualizations. At last, it is recommended the usage of the latest version.
# 
# It is worth mentioning that starting from version **0.1.3** rule property "coverage" was renamed to "support" to be consistent with logic rules literature.

# In[1]:


import numpy as np
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score


data_file = 'german-processed-iforest-data.csv'

feature_names = np.genfromtxt( './' + data_file, delimiter = ';', skip_header = 0, max_rows = 1, dtype = "str" )
feature_names = feature_names[ :-1 ]

dataset = np.loadtxt( './' + data_file, delimiter = ';', skiprows = 1 )

X = dataset[ :, :-1 ]
y = dataset[ :, -1 ]
y = y[ : ] - 1


target_names = np.array( [ 'approved', 'denied' ] )


sss = list( StratifiedShuffleSplit( n_splits = 1, test_size = 0.30, random_state = 1850 ).split( X, y ) )
train_indexes = sss[ 0 ][ 0 ]
test_indexes = sss[ 0 ][ 1 ]

X_train, X_test = X[ train_indexes ], X[ test_indexes ]
y_train, y_test = y[ train_indexes ], y[ test_indexes ]


kargs = eval( "{'criterion': 'gini', 'n_estimators': 32, 'max_depth': 6, 'random_state': 1850, 'bootstrap': False}" )
clf = RandomForestClassifier( **kargs )
clf.fit( X_train, y_train )


y_true, y_pred = y_test, clf.predict( X_test )
accuracy = accuracy_score( y_true, y_pred )
print( 'accuracy RF-32', accuracy )


# In[2]:


from exmatrix import ExplainableMatrix

exm = ExplainableMatrix( n_features = len( feature_names ), n_classes = len( target_names ), feature_names = np.array( feature_names ), class_names = np.array( target_names ) )
exm.rule_extration( clf, X, y, clf.feature_importances_ )
print( 'n_rules RF-32', exm.n_rules_ )


# In[3]:


exp = exm.explanation( r_order = 'class & coverage', f_order = 'importance', info_text = '\ntrees 32\nmax-depth 6\n\naccuracy 0.81\nerror 0.19\n' )
exp.create_svg( draw_col_labels = True, draw_cols_line = True, col_label_degrees = 12, height = 1000, margin_bottom = 200 )
exp.save( 'GermanCreditGE.png', pixel_scale = 5 )
exp.display_jn()


# In[4]:


exp = exm.explanation( r_coverage = 0.10, r_certainty = 0.75, r_order = 'class & coverage', f_order = 'importance' )
exp.create_svg( draw_col_labels = True, draw_cols_line = True, col_label_degrees = 12, height = 850, margin_bottom = 175 )
exp.save( 'GermanCreditGE-F.png', pixel_scale = 5 )
exp.display_jn()


# In[5]:


exp = exm.explanation( exp_type = 'local-used', x_k = X_test[ 154 ], r_order = 'class & certainty', f_order = 'importance', info_text = '\ninstance 154' )
exp.create_svg( draw_x_k = True, draw_change_line = True, draw_col_labels = True, draw_cols_line = True, col_label_degrees = 15, height = 800, margin_bottom = 210 )
exp.save( 'GermanCreditLEUR-154.png', pixel_scale = 5 )
exp.display_jn()


# In[6]:


exp = exm.explanation( exp_type = 'local-closest', x_k = X_test[ 154 ], r_order = 'class & certainty', f_order = 'importance', info_text = '\ninstance 154' )
exp.create_svg( draw_x_k = True, draw_deltas = True, draw_col_labels = True, draw_cols_line = True, cell_background = True, col_label_degrees = 15, height = 800, margin_bottom = 210 )
exp.save( 'GermanCreditLESC-154.png', pixel_scale = 5 )
exp.display_jn()


# In[7]:


i_credit_amount = 3
i_duration_of_credit = 1

X_test[ 154, i_credit_amount ] = 867
X_test[ 154, i_duration_of_credit ] = 15

exp = exm.explanation( exp_type = 'local-used', x_k = X_test[ 154 ], r_order = 'class & certainty', f_order = 'importance', info_text = '\ninstance 154' )
exp.create_svg( draw_x_k = True, draw_change_line = True, draw_col_labels = True, draw_cols_line = True, col_label_degrees = 15, height = 800, margin_bottom = 210 )
exp.save( 'GermanCreditLEUR-N154.png', pixel_scale = 5 )
exp.display_jn()

