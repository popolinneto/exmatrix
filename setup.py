from setuptools import setup


def readme():
    with open('README.md') as f:
        README = f.read()
    return README


version = '0.2.3'


setup(
    name = 'exmatrix',
    version = version,
    description = 'A Python package to the ExMatrix method, supporting Random Forest models interpretability.',
    long_description = readme(),
    long_description_content_type = 'text/markdown',
    url = 'https://gitlab.com/popolinneto/exmatrix',
    author = 'Mario Popolin Neto',
    license = 'Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International',
    classifiers = [
        'License :: Free for non-commercial use',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.6',
    ],
    package_dir = { '': 'src' },
    packages = [ 'exmatrix' ],
    python_requires = '>=3.6',
    install_requires = [ 'lrmatrix>=0.1.3' ],
)