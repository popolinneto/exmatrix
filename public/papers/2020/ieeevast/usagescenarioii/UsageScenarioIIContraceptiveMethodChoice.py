#!/usr/bin/env python
# coding: utf-8

# # Usage Scenario II:  Contraceptive Method
# 
# In this usage scenario, we utilize the Contraceptive Method Choice dataset from the UCI Machine Learning Repository.
# 
# ***Cite us***:  M. Popolin Neto and F. V. Paulovich, "Explainable Matrix - Visualization for Global and Local Interpretability of Random Forest Classification Ensembles," in IEEE Transactions on Visualization and Computer Graphics, vol. 27, no. 2, pp. 1427-1437, Feb. 2021, doi: 10.1109/TVCG.2020.3030354.
# 
# ***BibTeX:*** @article{PopolinNeto:2020:ExMatrix,
#     author={Popolin{ }Neto, Mário and Paulovich, Fernando V.},
#     journal={IEEE Transactions on Visualization and Computer Graphics}, 
#     title={Explainable Matrix - Visualization for Global and Local Interpretability of Random Forest Classification Ensembles}, 
#     year={2021},
#     volume={27},
#     number={2},
#     pages={1427-1437},
#     doi={10.1109/TVCG.2020.3030354}}
#     
# ***WARNING***: The code below must be used along with exmatrix package version **0.1.0**. 
# 
# Improvements and naive bugs have been addressed in new versions. Thus, such new versions will require specific changes in the code and will generate slightly different visualizations. At last, it is recommended the usage of the latest version.
# 
# It is worth mentioning that starting from version **0.1.3** rule property "coverage" was renamed to "support" to be consistent with logic rules literature.

# In[1]:


import numpy as np
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score


data_file = 'cmc.data'

feature_names = [ 'Wife age', 'Wife education', 'Husbands education', 'Number of children ever born', 'Wifes religion', 'Wife now working?', 'Husbands occupation', 'Standard-of-living index', 'Media exposure' ]
feature_names = np.array( feature_names )

dataset = np.genfromtxt( './' + data_file, delimiter = ',', skip_header = 0 )


X = dataset[ :, :-1 ]
y = dataset[ :, -1 ] - 1


target_names = np.array( [ 'No-use', 'Long-term', 'Short-term' ] )


sss = list( StratifiedShuffleSplit( n_splits = 1, test_size = 0.30, random_state = 3214 ).split( X, y ) )
train_indexes = sss[ 0 ][ 0 ]
test_indexes = sss[ 0 ][ 1 ]

X_train, X_test = X[ train_indexes ], X[ test_indexes ]
y_train, y_test = y[ train_indexes ], y[ test_indexes ]


kargs = eval( "{'criterion': 'gini', 'n_estimators': 32, 'max_depth': 6, 'random_state': 3214, 'bootstrap': False}" )
clf = RandomForestClassifier( **kargs )
clf.fit( X_train, y_train )


y_true, y_pred = y_test, clf.predict( X_test )
accuracy = accuracy_score( y_true, y_pred )
print( 'accuracy RF-32', accuracy )


# In[2]:


from exmatrix import ExplainableMatrix

exm = ExplainableMatrix( n_features = len( feature_names ), n_classes = len( target_names ), feature_names = np.array( feature_names ), class_names = np.array( target_names ) )
exm.rule_extration( clf, X, y, clf.feature_importances_ )
print( 'n_rules RF-32', exm.n_rules_ )


# In[3]:


exp = exm.explanation(  r_order = 'class & coverage', f_order = 'importance', info_text = '\ntrees 32\nmax-depth 6\n\naccuracy 0.63\nerror 0.37\n' )
exp.create_svg( draw_col_labels = True, draw_cols_line = True, col_label_degrees = 15, height = 1000, margin_bottom = 175 )
exp.save( 'ContraceptiveMethodChoiceGE.png', pixel_scale = 5 )
exp.display_jn()


# In[4]:


exp = exm.explanation( r_coverage = 0.10, r_certainty = 0.55, r_order = 'class & coverage', f_order = 'importance' )
exp.create_svg( draw_col_labels = True, draw_cols_line = True, col_label_degrees = 15,  height = 805, margin_bottom = 175 )
exp.save( 'ContraceptiveMethodChoiceGE-F.png', pixel_scale = 5 )
exp.display_jn()

